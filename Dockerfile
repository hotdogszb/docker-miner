FROM nvidia/cuda:11.2.1-base

RUN apt update && apt install -y --no-install-recommends libpci3 \
  && apt-get purge --autoremove -y curl \
  && rm -rf /var/lib/apt/lists/*

COPY . /app

CMD ["/app/start.sh"]